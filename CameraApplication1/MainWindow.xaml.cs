﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using EDSDKLib;

namespace CanonCamera
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        SDKHandler CameraHandler;
        List<int> AvList;
        List<int> TvList;
        List<int> ISOList;
        List<Camera> CamList;
        Bitmap Evf_Bmp;
       // int LVBw, LVBh;
        int w, h;
        float LVBratio, LVration;
        System.Windows.Threading.DispatcherTimer dispatcherTimer1 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer2 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer3 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer4 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer5 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer6 = new System.Windows.Threading.DispatcherTimer();
       
        [System.Runtime.InteropServices.DllImport("gdi32.dll", SetLastError = true)]

        private static extern bool DeleteObject(IntPtr hObject);

        int picNo = 0;

        public MainWindow()
        {
            InitializeComponent();
           CameraHandler = new SDKHandler();
             CameraHandler.CameraAdded += new SDKHandler.CameraAddedHandler(SDK_CameraAdded);
            CameraHandler.LiveViewUpdated += new SDKHandler.StreamUpdate(SDK_LiveViewUpdated);
            CameraHandler.ProgressChanged += new SDKHandler.ProgressHandler(SDK_ProgressChanged);
            CameraHandler.CameraHasShutdown += CameraHandler_CameraHasShutdown;
            SavePathTextBox.Text = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "RemotePhoto");
          //  LVBw = LiveViewPicBox.Width;
           // LVBh = LiveViewPicBox.Height;
            RefreshCamera();

            OpenSession();
            STComputerButton.IsChecked = true;
            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);
             CameraHandler.SetCapacity();

            CameraHandler.StartLiveView();
           // BrowseButton.Enabled = true;
            //SavePathTextBox.Enabled = true;
            dispatcherTimer5.Tick += new EventHandler(dispatcherTimer5_Tick);
            dispatcherTimer5.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer5.Start(); 
        }
        public static ImageSource ChangeBitmapToImageSource(Bitmap bitmap)
        {


            //Bitmap bitmap = icon.ToBitmap();
          //  IntPtr hBitmap = new IntPtr();
            IntPtr hBitmap = bitmap.GetHbitmap();


            ImageSource wpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(

                hBitmap,

                IntPtr.Zero,

                Int32Rect.Empty,

                BitmapSizeOptions.FromEmptyOptions());


            if (!DeleteObject(hBitmap))
            {

                throw new System.ComponentModel.Win32Exception();

            }


            return wpfBitmap;

        }
        private void dispatcherTimer5_Tick(object sender, EventArgs e)
        {

           // LiveViewPicBox.Source = ChangeBitmapToImageSource(Evf_Bmp);
            dispatcherTimer5.Stop(); 
        }
        private void RefreshCamera()
        {
            CloseSession();
           // CameraListBox.Items.Clear();
            CamList = CameraHandler.GetCameraList();
         //   foreach (Camera cam in CamList) CameraListBox.Items.Add(cam.Info.szDeviceDescription);
         //   if (CamList.Count > 0) CameraListBox.SelectedIndex = 0;
        }
        private void CloseSession()
        {
            CameraHandler.CloseSession();
           // AvCoBox.Items.Clear();
         //   TvCoBox.Items.Clear();
          //  ISOCoBox.Items.Clear();
         //   SettingsGroupBox.Enabled = false;
        //    LiveViewGroupBox.Enabled = false;
         //   SessionButton.Text = "Open Session";
        //    SessionLabel.Text = "No open session";
        }
        private void SDK_ProgressChanged(int Progress)
        {
          //  MainProgressBar.Value = Progress;
        }
        private void CameraHandler_CameraHasShutdown(object sender, EventArgs e)
        {
            CloseSession();
        }
        private void SDK_CameraAdded()
        {
            RefreshCamera();
        }
        private void SDK_LiveViewUpdated(Stream img)
        {
           // System.Windows.MessageBox.Show("test");
            Evf_Bmp = new Bitmap(img);
           
            this.LiveViewPicBox.Dispatcher.Invoke(
           new Action(
                delegate
                {
                    ImageSource evf_bitmap = ChangeBitmapToImageSource(Evf_Bmp);
                    this.LiveViewPicBox.Source = evf_bitmap;
                }
           )
           );
        }


        private void OpenSession()
        {
            if (CamList.Count > 0)
            { 
                CameraHandler.OpenSession(CamList[0]);
                //SessionButton.Text = "Close Session";
                string cameraname = CameraHandler.MainCamera.Info.szDeviceDescription;
              //  SessionLabel.Text = cameraname;
              //  if (CameraHandler.GetSetting(EDSDK.PropID_AEMode) != EDSDK.AEMode_Manual) MessageBox.Show("Camera is not in manual mode. Some features might not work!");
                AvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Av);
                TvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Tv);
                ISOList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_ISOSpeed);
             //   foreach (int Av in AvList) AvCoBox.Items.Add(CameraValues.AV((uint)Av));
             //   foreach (int Tv in TvList) TvCoBox.Items.Add(CameraValues.TV((uint)Tv));
            //    foreach (int ISO in ISOList) ISOCoBox.Items.Add(CameraValues.ISO((uint)ISO));
            //    AvCoBox.SelectedIndex = AvCoBox.Items.IndexOf(CameraValues.AV((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_Av)));
            //    TvCoBox.SelectedIndex = TvCoBox.Items.IndexOf(CameraValues.TV((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_Tv)));
           //     ISOCoBox.SelectedIndex = ISOCoBox.Items.IndexOf(CameraValues.ISO((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_ISOSpeed)));
                int wbidx = (int)CameraHandler.GetSetting((uint)EDSDK.PropID_WhiteBalance);
             }
            /*    switch (wbidx)
                {
                    case EDSDK.WhiteBalance_Auto: WBCoBox.SelectedIndex = 0; break;
                    case EDSDK.WhiteBalance_Daylight: WBCoBox.SelectedIndex = 1; break;
                    case EDSDK.WhiteBalance_Cloudy: WBCoBox.SelectedIndex = 2; break;
                    case EDSDK.WhiteBalance_Tangsten: WBCoBox.SelectedIndex = 3; break;
                    case EDSDK.WhiteBalance_Fluorescent: WBCoBox.SelectedIndex = 4; break;
                    case EDSDK.WhiteBalance_Strobe: WBCoBox.SelectedIndex = 5; break;
                    case EDSDK.WhiteBalance_WhitePaper: WBCoBox.SelectedIndex = 6; break;
                    case EDSDK.WhiteBalance_Shade: WBCoBox.SelectedIndex = 7; break;
                    default: WBCoBox.SelectedIndex = -1; break;
                }*/
            //    SettingsGroupBox.Enabled = true;
            //    LiveViewGroupBox.Enabled = true;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer1.Tick += new EventHandler(dispatcherTimer1_Tick);
            dispatcherTimer1.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer1.Start();
            dispatcherTimer2.Tick += new EventHandler(dispatcherTimer2_Tick);
            dispatcherTimer2.Interval = new TimeSpan(0, 0, 3);
            dispatcherTimer2.Start();
            dispatcherTimer3.Tick += new EventHandler(dispatcherTimer3_Tick);
            dispatcherTimer3.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimer3.Start();
            dispatcherTimer4.Tick += new EventHandler(dispatcherTimer4_Tick);
            dispatcherTimer4.Interval = new TimeSpan(0, 0, 7);
            dispatcherTimer4.Start();
            dispatcherTimer6.Tick += new EventHandler(dispatcherTimer6_Tick);
            dispatcherTimer6.Interval = new TimeSpan(0, 0, 12);
            dispatcherTimer6.Start();
        }
        private void dispatcherTimer1_Tick(object sender, EventArgs e)
        {
            //if (STComputerButton.Checked || STBothButton.Checked) 
            Directory.CreateDirectory(SavePathTextBox.Text);
            CameraHandler.ImageSaveDirectory = SavePathTextBox.Text;
            //  if ((string)TvCoBox.SelectedItem == "Bulb") CameraHandler.TakePhoto((uint)BulbUpDo.Value);
            //   else 
            CameraHandler.TakePhoto();
            dispatcherTimer1.Stop();
        }
        private void dispatcherTimer2_Tick(object sender, EventArgs e)
        {
            //if (STComputerButton.Checked || STBothButton.Checked) 
            Directory.CreateDirectory(SavePathTextBox.Text);
            CameraHandler.ImageSaveDirectory = SavePathTextBox.Text;
            //  if ((string)TvCoBox.SelectedItem == "Bulb") CameraHandler.TakePhoto((uint)BulbUpDo.Value);
            //   else 
            CameraHandler.TakePhoto();
            dispatcherTimer2.Stop();
        }
        private void dispatcherTimer3_Tick(object sender, EventArgs e)
        {
            //if (STComputerButton.Checked || STBothButton.Checked) 
            Directory.CreateDirectory(SavePathTextBox.Text);
            CameraHandler.ImageSaveDirectory = SavePathTextBox.Text;
            //  if ((string)TvCoBox.SelectedItem == "Bulb") CameraHandler.TakePhoto((uint)BulbUpDo.Value);
            //   else 
            CameraHandler.TakePhoto();
            dispatcherTimer3.Stop();
        }
        private void dispatcherTimer4_Tick(object sender, EventArgs e)
        {
            //String[] imageFilePaths = new String[] { "SavePathTextBox.Text\\IMG_0001.JPG", "SavePathTextBox.Text\\IMG_0002.JPG", "SavePathTextBox.Text\\IMG_0003.JPG" };
            MakeThumbnail(SavePathTextBox.Text + "\\IMG_0001.JPG", SavePathTextBox.Text +"\\IMG_0001_min.JPG",640,480);
            MakeThumbnail(SavePathTextBox.Text + "\\IMG_0002.JPG", SavePathTextBox.Text + "\\IMG_0002_min.JPG", 640, 480);
            MakeThumbnail(SavePathTextBox.Text + "\\IMG_0003.JPG", SavePathTextBox.Text + "\\IMG_0003_min.JPG", 640, 480);        
            dispatcherTimer4.Stop();
        }
        private void dispatcherTimer6_Tick(object sender, EventArgs e)
        {
            String[] imageFilePaths = new String[] { SavePathTextBox.Text  + "\\IMG_0001_min.JPG", SavePathTextBox.Text + "\\IMG_0002_min.JPG", SavePathTextBox.Text + "\\IMG_0003_min.JPG" };
            String outputFilePath = SavePathTextBox.Text + "\\test.gif";
            AnimatedGifEncoder age = new AnimatedGifEncoder();
            age.Start(outputFilePath);
            age.SetDelay(500);
            //-1:no repeat,0:always repeat
            age.SetRepeat(0);
            for (int i = 0, count = imageFilePaths.Length; i < count; i++)
            {
                age.AddFrame(System.Drawing.Image.FromFile(imageFilePaths[i]));
            }
            age.Finish();
            /* extract Gif */
            //string outputPath = "c:\\";
            GifDecoder gifDecoder = new GifDecoder();
            gifDecoder.Read("c:\\test.gif");
            for (int i = 0, count = gifDecoder.GetFrameCount(); i < count; i++)
            {
                System.Drawing.Image frame = gifDecoder.GetFrame(i);  // frame i
                //frame.Save( outputPath + Guid.NewGuid().ToString() + ".png", ImageFormat.Png );
            }
            dispatcherTimer6.Stop();
        }
        public void MakeThumbnail(string originalImagePath, string thumbnailPath, int width, int height)
        {

            System.Drawing.Image originalImage = System.Drawing.Image.FromFile(originalImagePath);

            int towidth = width;
            int toheight = height;

            int x = 0;
            int y = 0;
            int ow = originalImage.Width;
            int oh = originalImage.Height;

   

            //新建一个bmp图片 
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

            //新建一个画板 
            Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            //设置高质量插值法 
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

            //设置高质量,低速度呈现平滑程度 
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //清空画布并以透明背景色填充 
            g.Clear(System.Drawing.Color.Transparent);

            //在指定位置并且按指定大小绘制原图片的指定部分 
            g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, towidth, toheight), new System.Drawing.Rectangle(x, y, ow, oh), GraphicsUnit.Pixel);

            try
            {
                System.Drawing.Imaging.ImageFormat imgtype = System.Drawing.Imaging.ImageFormat.Jpeg;
                //保存缩略图
                bitmap.Save(thumbnailPath, imgtype);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                originalImage.Dispose();
                bitmap.Dispose();
                g.Dispose();
            }
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CameraHandler.StartLiveView();
        }
    }
}
